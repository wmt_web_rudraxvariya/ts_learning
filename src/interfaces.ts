export interface AppProps {
    name: String;
    add?: number;
}
export interface User {
    name: string;
    age: number
}
export interface Name {
    firstName: string;
}
export interface Status {
    status: string;
}
export interface UserData {
    id: number;
    name: string;
    age: number;
}