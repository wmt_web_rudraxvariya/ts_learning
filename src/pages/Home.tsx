import * as React from 'react';
import { Link } from 'react-router-dom';

interface IHomeProps {}

const Home: React.FunctionComponent<IHomeProps> = () => {
    return (
        <>
            <div>
                <Link to="/form/2">
                    <h2>Form</h2>
                </Link>
                <Link to="/generic">
                    <h2>generic</h2>
                </Link>
                <Link to="/state">
                    <h2>state</h2>
                </Link>
                <Link to="/type">
                    <h2>type</h2>
                </Link>
                <Link to="/test">
                    <h2>test</h2>
                </Link>
            </div>
        </>
    );
};

export default Home;
