import React from "react";
import { User } from "../interfaces";

const StateLearning = () => {
  const [first, setFirst] = React.useState<User | null>({
    name: "james",
    age: 30,
  });

  return (
    <div>
      {first?.name}
      {first?.age}
      <button
        onClick={() => {
          setFirst({ name: "herry", age: 12 });
          console.log(first);
        }}
      >
        Click here
      </button>
    </div>
  );
};

export default StateLearning;
