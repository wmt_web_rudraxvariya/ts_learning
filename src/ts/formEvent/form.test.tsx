import { screen, render, fireEvent } from '@testing-library/react';
import FormEventsLearning from './FormEventsLearning';

it('type of hello must be a heading', () => {
    render(<FormEventsLearning />);
    const textField = screen.getByPlaceholderText(/Enter your name/) as HTMLInputElement;
    fireEvent.change(textField, { target: { value: 'Hesllo' } });
    expect(textField.value).toBe('Hesllo');
});
