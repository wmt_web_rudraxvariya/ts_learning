import React, { FC } from 'react';
import { Name } from '../../interfaces';
import { useParams } from 'react-router';

const FormEventsLearning: FC = () => {
    const [name, setName] = React.useState<Name | null>({
        firstName: ''
    });
    const { id } = useParams();
    const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setName({
            firstName: e.target.value
        });
    };
    const handleFormSubmit = (e: React.ChangeEvent<HTMLFormElement>) => {
        console.log(e);
    };
    const styles = {
        outerDiv: {
            width: 300,
            height: 300,
            margin: '50px auto',
            backgroundColor: 'orange',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            fontWeight: 'bold',
            padding: '10px'
        }
    } as const;
    const handlebtnForm = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
        e.preventDefault();
        console.log(name);
    };
    return (
        <div style={styles.outerDiv}>
            <form onSubmit={handleFormSubmit}>
                <input type="text" placeholder="Enter your name" value={name?.firstName} onChange={handleInputChange} />
                <button type="submit" onClick={handlebtnForm}>
                    Submit
                </button>
                <br />
                <h3>Your number is {id && id}</h3>
            </form>
            <h1>{name?.firstName}</h1>
        </div>
    );
};

export default FormEventsLearning;
