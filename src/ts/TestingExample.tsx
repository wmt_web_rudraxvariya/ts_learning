import * as React from 'react';

export interface IRouteExampleProps {}

const TestingExample: React.FunctionComponent<IRouteExampleProps> = (props) => {
    return (
        <div>
            <h1>Hello</h1>
        </div>
    );
};
export default TestingExample;
