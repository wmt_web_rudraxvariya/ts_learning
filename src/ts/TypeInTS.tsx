import React from "react";

const TypeInTS = () => {
  //   type CheckoutSteps = "" | "DeliveryMethod" | "PaymentMethod" | "Review";
  enum CheckoutSteps {
    Initial,
    DeliveryMethod,
    PaymentMethod,
    Review,
  }
  const [name, setName] = React.useState<CheckoutSteps>(CheckoutSteps.Initial);
  console.log(name);

  return (
    <>
      {name === CheckoutSteps.Initial && (
        <>
          <h1>ordered</h1>
          <button onClick={() => setName(CheckoutSteps.DeliveryMethod)}>
            Next
          </button>
        </>
      )}
      {name === CheckoutSteps.DeliveryMethod && (
        <>
          <h1>DeliveryMethod</h1>
          <button onClick={() => setName(CheckoutSteps.PaymentMethod)}>
            Next
          </button>
        </>
      )}
      {name === CheckoutSteps.PaymentMethod && (
        <>
          <h1>PaymentMethod</h1>
          <button onClick={() => setName(CheckoutSteps.Review)}>Next</button>
        </>
      )}
      {name === CheckoutSteps.Review && <h1>Review</h1>}
    </>
  );
};

export default TypeInTS;
