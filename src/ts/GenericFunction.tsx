import React from "react";

interface ID {
  id: number;
}

interface DataProps<T> {
  items: T[];
}

export default function GenericFunction<T extends ID>({ items }: DataProps<T>) {
  return (
    <ul>
      {items.map((item) => (
        <li key={item.id}>{JSON.stringify(item)}</li>
      ))}
    </ul>
  );
}
