import React from 'react';
import './App.css';
import { AppProps } from './interfaces';
import FormEventsLearning from './ts/formEvent/FormEventsLearning';
import GenericFunction from './ts/GenericFunction';
import StateLearning from './ts/StateLearning';
import TypeInTS from './ts/TypeInTS';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Home from './pages/Home';
import TestingExample from './ts/TestingExample';

function App({ name, add }: AppProps) {
    const user = [
        { id: 1, name: 'Rahul', age: 25 },
        { id: 2, name: 'Raj', age: 25 },
        { id: 3, name: 'Ravi', age: 25 },
        { id: 4, name: 'Rajesh', age: 25 },
        { id: 5, name: 'Raju', age: 25 }
    ];
    const anotherData = [
        { id: 1, name: 'Rahul', age: 25 },
        { id: 2, name: 'Raj', age: 25 },
        { id: 3, name: 'Ravi', age: 25 }
    ];
    return (
        <div className="App">
            <Router>
                <Routes>
                    <Route path="/" element={<Home />} />
                    <Route path="/form" element={<FormEventsLearning />} />
                    <Route path="/form/:id" element={<FormEventsLearning />} />
                    <Route path="/generic" element={<GenericFunction items={anotherData} />} />
                    <Route path="/state" element={<StateLearning />} />
                    <Route path="/type" element={<TypeInTS />} />
                    <Route path="/test" element={<TestingExample />} />
                </Routes>
            </Router>
        </div>
    );
}

export default App;
