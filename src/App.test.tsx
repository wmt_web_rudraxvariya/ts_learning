import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

it('must contain hello', () => {
    render(<App name="Hello" />);
    const linkElement = screen.queryByText('Hello');
    expect(linkElement).toBeNull();
});
