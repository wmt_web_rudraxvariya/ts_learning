import React from 'react';
import { render, screen } from '@testing-library/react';
import Navbar from '../Navbar';
describe('Header test', () => {
    beforeAll(() => {
        console.log('first');
    });
    test('renders learn react link', () => {
        render(<Navbar />);
        let linkElement = screen.getByText(/Header/i);
        expect(linkElement).toBeInTheDocument();
    });

    it('type of hello must be a heading', () => {
        render(<Navbar />);
        const linkElement = screen.getByTestId('header');
        expect(linkElement).toBeInTheDocument();
    });

    it('find of hello must be a heading', async () => {
        render(<Navbar />);
        const linkElement = await screen.findByTestId('header');
        expect(linkElement).toBeInTheDocument();
    });
    it('Get all by role', () => {
        render(<Navbar />);
        const linkElement = screen.getAllByRole('heading');
        expect(linkElement.length).toBe(1);
    });
});
